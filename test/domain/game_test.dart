import 'package:bacongamejam20198/domain/action.dart';
import 'package:bacongamejam20198/domain/damage.dart';
import 'package:bacongamejam20198/domain/game.dart';
import 'package:bacongamejam20198/domain/move/move-punch.dart';
import 'package:bacongamejam20198/domain/move/move-tackle.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../mocks/creatures.dart';
import '../mocks/games.dart';

void main() {
  group("Game", () {
    Game game;

    setUp(() {
      game = mockGame();
    });

    test("is not over initially", () => expect(game.gameOver, false));

    test("is not tied", () => expect(game.tie, false));

    test("has no winner", () => expect(game.winner, null));

    test("has no loser", () => expect(game.loser, null));

    group("with duplicate action", () {
      test(
          "throws exception",
          () => expect(
              () => game.performTurn([
                    ActionPass(casterIndex: 0),
                    ActionPass(casterIndex: 0),
                  ]),
              throwsA(isInstanceOf<Exception>())));
    });

    group("with missing action", () {
      test(
          "throws exception",
          () => expect(
              () => game.performTurn([
                    ActionPass(casterIndex: 0),
                  ]),
              throwsA(isInstanceOf<Exception>())));
    });

    group("when a player passes", () {
      setUp(() {
        game.performTurn([
          ActionPass(casterIndex: 0),
          ActionPass(casterIndex: 1),
        ]);
      });

      test(
          "events are emitted on active creature",
          () => verifyInOrder([
                game.players[0].activeCreature.turnStart(),
                game.players[0].activeCreature.pass(),
                game.players[0].activeCreature.turnEnd(),
              ]));
    });

    group("when boths players attack", () {
      MockCreature player1Creature;
      MockCreature player2Creature;

      setUp(() {
        player1Creature = game.players[0].activeCreature;
        player2Creature = game.players[1].activeCreature;
        when(player1Creature.moves).thenReturn([MoveTackle()]);
        when(player2Creature.moves).thenReturn([MovePunch()]);
        game.performTurn([
          ActionMove(casterIndex: 0, moveIndex: 0),
          ActionMove(casterIndex: 1, moveIndex: 0),
        ]);
      });

      test(
          "events are emitted on active creature of player 1",
          () => verifyInOrder([
                player1Creature.turnStart(),
                player1Creature.executeMove(argThat(isA<MoveTackle>())),
                player1Creature.inflictDamage(argThat(isA<Damage>())),
                player1Creature.turnEnd(),
              ]));

      test(
          "events are emitted on active creature of player 2",
          () => verifyInOrder([
                player2Creature.turnStart(),
                player2Creature.executeMove(argThat(isA<MovePunch>())),
                player2Creature.inflictDamage(argThat(isA<Damage>())),
                player2Creature.turnEnd(),
              ]));

      test("active creature of player 1 receives correct damage", () {
        expect(
            verify(player1Creature.inflictDamage(captureAny))
                .captured
                .single
                .amount,
            5);
      });

      test("active creature of player 2 receives correct damage", () {
        expect(
            verify(player2Creature.inflictDamage(captureAny))
                .captured
                .single
                .amount,
            5);
      });
    });

    group("when a player changes creatures and other player attacks", () {
      MockCreature newCreature;
      MockCreature oldCreature;

      setUp(() {
        oldCreature = game.players[0].creatures[0];
        newCreature = mockCreatureMockito();
        game.players[0].creatures.insert(1, newCreature);
        when(game.players[1].activeCreature.moves).thenReturn([MovePunch()]);
        game.performTurn([
          ActionChangeCreature(casterIndex: 0, creatureIndex: 1),
          ActionMove(casterIndex: 1, moveIndex: 0),
        ]);
      });

      test(
          "events are emitted on active creature",
          () => verifyInOrder([
                oldCreature.turnStart(),
                oldCreature.dismiss(),
                newCreature.summon(),
                newCreature.inflictDamage(argThat(isA<Damage>())),
                newCreature.turnEnd(),
              ]));
    });

    group("when A surrenders and B passes", () {
      setUp(() {
        game.performTurn([
          ActionPass(casterIndex: 0),
          ActionSurrender(casterIndex: 1),
        ]);
      });

      test("is over", () => expect(game.gameOver, true));

      test("is not tied", () => expect(game.tie, false));

      test("player 1 is winner", () => expect(game.winner, game.players[0]));

      test("player 2 is loser", () => expect(game.loser, game.players[1]));
    });
  });
}
