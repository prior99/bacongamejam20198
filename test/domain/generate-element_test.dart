import 'package:bacongamejam20198/domain/element.dart';
import 'package:bacongamejam20198/domain/generator/generate-element.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("generateElement", () {
    test("generates stable element for seed 1",
        () => expect(generateElement(seed: 1), Element.ANIMAL));

    test("generates stable element for seed 2",
        () => expect(generateElement(seed: 2), Element.CHAOS));

    test("generates stable element for seed 3",
        () => expect(generateElement(seed: 3), Element.VEGETABLE));

    test("generates stable element for seed 4",
        () => expect(generateElement(seed: 4), Element.HUMAN));
  });
}
