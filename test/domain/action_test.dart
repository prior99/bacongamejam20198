import 'package:bacongamejam20198/domain/action.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("ActionSurrender", () {
    Action action;
    Map<String, dynamic> expected = {
      "actionType": "SURRENDER",
      "casterIndex": 1,
    };

    setUp(() {
      action = ActionSurrender(casterIndex: 1);
    });

    test("when serializing", () => expect(action.toJson(), expected));

    test("when deserializing", () {
      final result = Action.fromJson(expected);
      expect(result, isInstanceOf<ActionSurrender>());
      expect(result.actionType, ActionType.SURRENDER);
      expect(result.casterIndex, 1);
    });
  });

  group("ActionChangeCreature", () {
    Action action;
    Map<String, dynamic> expected = {
      "actionType": "CHANGE_CREATURE",
      "casterIndex": 1,
      "creatureIndex": 2,
    };

    setUp(() {
      action = ActionChangeCreature(casterIndex: 1, creatureIndex: 2);
    });

    test("when serializing", () => expect(action.toJson(), expected));

    test("when deserializing", () {
      final result = Action.fromJson(expected);
      expect(result, isInstanceOf<ActionChangeCreature>());
      expect(result.actionType, ActionType.CHANGE_CREATURE);
      expect(result.casterIndex, 1);
      expect((result as ActionChangeCreature).creatureIndex, 2);
    });
  });

  group("ActionMove", () {
    Action action;
    Map<String, dynamic> expected = {
      "actionType": "MOVE",
      "casterIndex": 1,
      "moveIndex": 2,
    };

    setUp(() {
      action = ActionMove(casterIndex: 1, moveIndex: 2);
    });

    test("when serializing", () => expect(action.toJson(), expected));

    test("when deserializing", () {
      final result = Action.fromJson(expected);
      expect(result, isInstanceOf<ActionMove>());
      expect(result.actionType, ActionType.MOVE);
      expect(result.casterIndex, 1);
      expect((result as ActionMove).moveIndex, 2);
    });
  });

  group("ActionPass", () {
    Action action;
    Map<String, dynamic> expected = {
      "actionType": "PASS",
      "casterIndex": 1,
    };

    setUp(() {
      action = ActionPass(casterIndex: 1);
    });

    test("when serializing", () => expect(action.toJson(), expected));

    test("when deserializing", () {
      final result = Action.fromJson(expected);
      expect(result, isInstanceOf<ActionPass>());
      expect(result.actionType, ActionType.PASS);
      expect(result.casterIndex, 1);
    });
  });
}
