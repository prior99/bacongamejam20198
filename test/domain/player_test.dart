import 'package:bacongamejam20198/domain/player.dart';
import 'package:flutter_test/flutter_test.dart';

import '../mocks/players.dart';

void main() {
  group("Player", () {
    Player player;

    setUp(() {
      player = mockPlayerA();
    });

    test("is not defeated initially", () => expect(player.defeated, false));

    test("has first creature active", () => expect(player.activeCreature, player.creatures[0]));
  });
}