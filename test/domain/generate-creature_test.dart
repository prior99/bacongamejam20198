import 'package:bacongamejam20198/domain/element.dart';
import 'package:bacongamejam20198/domain/generator/generate-creature.dart';
import 'package:bacongamejam20198/domain/move/move-bandage.dart';
import 'package:bacongamejam20198/domain/move/move-block.dart';
import 'package:bacongamejam20198/domain/move/move-cute-look.dart';
import 'package:bacongamejam20198/domain/move/move-electrical-discharge.dart';
import 'package:bacongamejam20198/domain/move/move-poison.dart';
import 'package:bacongamejam20198/domain/move/move-punch.dart';
import 'package:bacongamejam20198/domain/move/move-tackle.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("generateCreature", () {
    test("generates stable creature for seed 1", () {
      final creature = generateCreature(seed: 1);
      expect(creature.element, Element.ANIMAL);
      expect(creature.moves, [
        isInstanceOf<MoveCuteLook>(),
        isInstanceOf<MovePunch>(),
        isInstanceOf<MoveBlock>(),
        isInstanceOf<MoveTackle>(),
      ]);

      // Snapshot values.
      expect(creature.baseAtk, 6);
      expect(creature.baseDef, 14);
      expect(creature.baseMaxHealth, 80);
      expect(creature.baseMaxStamina, 7);

      // Consistency check.
      expect(creature.baseAtk + creature.baseDef, 20);
      expect(creature.baseMaxStamina * 10 + creature.baseMaxHealth, 150);
    });

    test("generates stable creature for seed 2", () {
      final creature = generateCreature(seed: 2);
      expect(creature.element, Element.CHAOS);
      expect(creature.moves, [
        isInstanceOf<MovePunch>(),
        isInstanceOf<MoveElectricalDischarge>(),
        isInstanceOf<MoveTackle>(),
        isInstanceOf<MoveBlock>(),
      ]);

      // Snapshot values.
      expect(creature.baseAtk, 11);
      expect(creature.baseDef, 9);
      expect(creature.baseMaxHealth, 100);
      expect(creature.baseMaxStamina, 5);

      // Consistency check.
      expect(creature.baseAtk + creature.baseDef, 20);
      expect(creature.baseMaxStamina * 10 + creature.baseMaxHealth, 150);
    });

    test("generates stable creature for seed 3", () {
      final creature = generateCreature(seed: 3);
      expect(creature.element, Element.VEGETABLE);
      expect(creature.moves, [
        isInstanceOf<MoveBlock>(),
        isInstanceOf<MoveTackle>(),
        isInstanceOf<MovePoison>(),
        isInstanceOf<MovePunch>(),
      ]);

      // Snapshot values.
      expect(creature.baseAtk, 9);
      expect(creature.baseDef, 11);
      expect(creature.baseMaxHealth, 80);
      expect(creature.baseMaxStamina, 7);

      // Consistency check.
      expect(creature.baseAtk + creature.baseDef, 20);
      expect(creature.baseMaxStamina * 10 + creature.baseMaxHealth, 150);
    });

    test("generates stable creature for seed 4", () {
      final creature = generateCreature(seed: 4);
      expect(creature.element, Element.HUMAN);
      expect(creature.moves, [
        isInstanceOf<MoveTackle>(),
        isInstanceOf<MoveBandage>(),
        isInstanceOf<MoveBlock>(),
        isInstanceOf<MovePunch>(),
      ]);

      // Snapshot values.
      expect(creature.baseAtk, 4);
      expect(creature.baseDef, 16);
      expect(creature.baseMaxHealth, 100);
      expect(creature.baseMaxStamina, 5);

      // Consistency check.
      expect(creature.baseAtk + creature.baseDef, 20);
      expect(creature.baseMaxStamina * 10 + creature.baseMaxHealth, 150);
    });
  });
}
