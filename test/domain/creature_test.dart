import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/damage.dart';
import 'package:bacongamejam20198/domain/effect/effect-charmed.dart';
import 'package:bacongamejam20198/domain/effect/effect-interval-heal.dart';
import 'package:bacongamejam20198/domain/element.dart';
import 'package:bacongamejam20198/domain/event.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../mocks/creatures.dart';
import '../mocks/effects.dart';

void main() {
  group("Creature", () {
    Creature creature;

    setUp(() {
      creature = mockCreatureVegetable();
    });

    test("is not dead initially", () => expect(creature.dead, false));

    group("with mock effect", () {
      MockEffect effect;

      setUp(() {
        effect = MockEffect();
        when(effect.changeAtk(target: anyNamed("target"))).thenReturn(10);
        when(effect.changeDef(target: anyNamed("target"))).thenReturn(-10);
        when(effect.changeMaxHealth(target: anyNamed("target"))).thenReturn(-5);
        when(effect.changeMaxStamina(target: anyNamed("target"))).thenReturn(5);
        creature.addEffect(effect);
      });

      test("adds ATK change", () => expect(creature.atk, 20));

      test("adds DEF change", () => expect(creature.def, 0));

      test("adds max health change", () => expect(creature.maxHealth, 95));

      test("adds max stamina change", () => expect(creature.maxStamina, 10));

      group("when summoned", () {
        setUp(() => creature.summon());

        test(
            "emits summoned event",
            () => verify(effect.onEvent(
                  target: argThat(same(creature), named: "target"),
                  event: argThat(isA<EventSummoned>(), named: "event"),
                )).called(1));
      });

      group("when dismissed", () {
        setUp(() => creature.dismiss());

        test(
            "emits dismissed event",
            () => verify(effect.onEvent(
                    target: argThat(same(creature), named: "target"),
                    event: argThat(isA<EventDismiss>(), named: "event")))
                .called(1));
      });

      group("when damage is inflicted", () {
        Damage damage;

        setUp(() {
          damage = Damage(amount: 1, element: Element.ANIMAL);
          creature.inflictDamage(damage);
        });

        test(
            "emits damage event",
            () => verify(effect.onEvent(
                    target: argThat(same(creature), named: "target"),
                    event: argThat(isA<EventDamage>(), named: "event")))
                .called(1));

        test(
            "damage event has correct damage",
            () => expect(
                verify(effect.onEvent(
                        target: anyNamed("target"),
                        event: captureAnyNamed("event")))
                    .captured
                    .single
                    .damage,
                damage));
      });

      group("when lethal damage is inflicted", () {
        Damage damage;

        setUp(() {
          damage = Damage(amount: 1000, element: Element.ANIMAL);
          creature.inflictDamage(damage);
        });

        test(
            "emits summoned event",
            () => verifyInOrder([
                  effect.onEvent(
                      target: argThat(same(creature), named: "target"),
                      event: argThat(isA<EventDamage>(), named: "event")),
                  effect.onEvent(
                      target: argThat(same(creature), named: "target"),
                      event: argThat(isA<EventPreDeath>(), named: "event"))
                ]));
      });

      group("when passing", () {
        setUp(() => creature.pass());

        test(
            "emits pass event",
            () => verify(effect.onEvent(
                    target: argThat(same(creature), named: "target"),
                    event: argThat(isA<EventPass>(), named: "event")))
                .called(1));
      });

      group("when turn starts", () {
        setUp(() => creature.turnStart());

        test(
            "emits turn start event",
            () => verify(effect.onEvent(
                    target: argThat(same(creature), named: "target"),
                    event: argThat(isA<EventTurnStart>(), named: "event")))
                .called(1));
      });

      group("when turn ends", () {
        setUp(() => creature.turnEnd());

        test(
            "emits turn end event",
            () => verify(effect.onEvent(
                    target: argThat(same(creature), named: "target"),
                    event: argThat(isA<EventTurnEnd>(), named: "event")))
                .called(1));
      });
    });
  });
}
