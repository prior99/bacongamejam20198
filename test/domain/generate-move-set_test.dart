import 'package:bacongamejam20198/domain/element.dart';
import 'package:bacongamejam20198/domain/generator/generate-move-set.dart';
import 'package:bacongamejam20198/domain/move/move-bandage.dart';
import 'package:bacongamejam20198/domain/move/move-block.dart';
import 'package:bacongamejam20198/domain/move/move-cute-look.dart';
import 'package:bacongamejam20198/domain/move/move-electrical-discharge.dart';
import 'package:bacongamejam20198/domain/move/move-poison.dart';
import 'package:bacongamejam20198/domain/move/move-punch.dart';
import 'package:bacongamejam20198/domain/move/move-tackle.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("generateMoveSet", () {
    test("generates stable move set for human element",
        () => expect(generateMoveSet(element: Element.HUMAN, seed: 17), [
          isInstanceOf<MoveTackle>(),
          isInstanceOf<MoveBandage>(),
          isInstanceOf<MoveBlock>(),
          isInstanceOf<MovePunch>(),
        ]));

    test("generates stable move set for chaos element",
        () => expect(generateMoveSet(element: Element.CHAOS, seed: 17), [
          isInstanceOf<MoveTackle>(),
          isInstanceOf<MoveElectricalDischarge>(),
          isInstanceOf<MoveBlock>(),
          isInstanceOf<MovePunch>(),
        ]));

    test("generates stable move set for animal element",
        () => expect(generateMoveSet(element: Element.ANIMAL, seed: 17), [
          isInstanceOf<MoveTackle>(),
          isInstanceOf<MoveCuteLook>(),
          isInstanceOf<MoveBlock>(),
          isInstanceOf<MovePunch>(),
        ]));

    test("generates stable move set for vegetable element",
        () => expect(generateMoveSet(element: Element.VEGETABLE, seed: 17), [
          isInstanceOf<MoveTackle>(),
          isInstanceOf<MovePoison>(),
          isInstanceOf<MoveBlock>(),
          isInstanceOf<MovePunch>(),
        ]));
  });
}
