import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/element.dart';
import 'package:bacongamejam20198/domain/move/move-electrical-discharge.dart';
import 'package:bacongamejam20198/domain/move/move-poison.dart';
import 'package:bacongamejam20198/domain/move/move-punch.dart';
import 'package:bacongamejam20198/domain/move/move-tackle.dart';
import 'package:mockito/mockito.dart';

class MockCreature extends Mock implements Creature {}

MockCreature mockCreatureMockito() {
  final creature = MockCreature();
  when(creature.dead).thenReturn(false);
  when(creature.def).thenReturn(10);
  when(creature.atk).thenReturn(10);
  return creature;
}

final mockCreatureChaos = () => Creature(
      baseAtk: 10,
      baseDef: 10,
      baseMaxHealth: 100,
      baseMaxStamina: 5,
      element: Element.CHAOS,
      moves: [
        MovePunch(),
        MoveTackle(),
        MoveElectricalDischarge(),
      ],
    );

final mockCreatureHuman = () => Creature(
      baseAtk: 10,
      baseDef: 10,
      baseMaxHealth: 100,
      baseMaxStamina: 5,
      element: Element.HUMAN,
      moves: [
        MovePunch(),
        MoveTackle(),
      ],
    );

final mockCreatureVegetable = () => Creature(
      baseAtk: 10,
      baseDef: 10,
      baseMaxHealth: 100,
      baseMaxStamina: 5,
      element: Element.VEGETABLE,
      moves: [
        MovePunch(),
        MoveTackle(),
        MovePoison(),
      ],
    );

final mockCreatureAnimal = () => Creature(
      baseAtk: 10,
      baseDef: 10,
      baseMaxHealth: 100,
      baseMaxStamina: 5,
      element: Element.ANIMAL,
      moves: [
        MovePunch(),
        MoveTackle(),
      ],
    );
