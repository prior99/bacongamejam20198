import 'package:bacongamejam20198/domain/game.dart';

import 'players.dart';

final mockGame = () => Game(players: [mockPlayerA(), mockPlayerB()]);

final mockGameNoMockito = () => Game(players: [mockPlayerNoMockitoA(), mockPlayerNoMockitoB()]);