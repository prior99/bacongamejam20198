import 'package:bacongamejam20198/domain/effect/effect.dart';
import 'package:mockito/mockito.dart';

class MockEffect extends Mock implements Effect {}

final mockEffectMockito = () => MockEffect();