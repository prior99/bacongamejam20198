import 'package:bacongamejam20198/domain/player.dart';

import 'creatures.dart';

final mockPlayerA = () => Player(creatures: [mockCreatureMockito(), mockCreatureAnimal(), mockCreatureChaos()]);

final mockPlayerB = () => Player(creatures: [mockCreatureMockito(), mockCreatureVegetable(), mockCreatureHuman()]);

final mockPlayerNoMockitoA = () => Player(creatures: [mockCreatureAnimal(), mockCreatureChaos()]);

final mockPlayerNoMockitoB = () => Player(creatures: [mockCreatureVegetable(), mockCreatureHuman()]);