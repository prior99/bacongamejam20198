import 'package:bacongamejam20198/stores/all.dart';
import 'package:bacongamejam20198/stores/application-state.dart';
import 'package:bacongamejam20198/utils/random.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class PreGame extends StatelessWidget {
  PreGame() {
    while (!creatures.complete) {
      creatures.addCreatureFromSeed(anyInt());
    }
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Column(children: [
      Observer(
          builder: (_) => Text(
              'in game: ${applicationState.gameId} - opponent connected: ${applicationState.hasOpponent} - ready: ${applicationState.current == ApplicationState.ready}/${applicationState.isOpponentReady}')),
      RaisedButton(
        child: Text('ready'),
        onPressed: applicationState.ready,
      )
    ]);
  }
}
