import 'package:bacongamejam20198/stores/all.dart';
import 'package:flutter/material.dart';

class JoinView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Join game'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              maxLines: 1,
              minLines: 1,
              textAlign: TextAlign.center,
              onChanged: applicationState.updateGameId,
              onEditingComplete: applicationState.join,
            ),
            RaisedButton(
              child: Text('Really Join'),
              autofocus: false,
              clipBehavior: Clip.none,
              onPressed: applicationState.join,
            )
          ],
        ),
      ),
    );
  }
}
