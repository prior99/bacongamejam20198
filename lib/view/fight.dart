import 'package:bacongamejam20198/domain/element.dart' as prefix0;
import 'package:bacongamejam20198/domain/player.dart';
import 'package:bacongamejam20198/stores/all.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

const Map<prefix0.Element, Color> ElementColors = {
  prefix0.Element.ANIMAL: Colors.yellowAccent,
  prefix0.Element.HUMAN: Colors.redAccent,
  prefix0.Element.VEGETABLE: Colors.greenAccent,
  prefix0.Element.CHAOS: Colors.blueGrey,
  prefix0.Element.NONE: Colors.grey,
};

class FightView extends StatelessWidget {
  FightView() {
    // TODO create player2 correctly from network
    gameState.initialize(
      players: [
        new Player(creatures: creatures.creatures),
        new Player(creatures: creatures.creatures)
      ],
      ownPlayerIndex: applicationState.isHost ? 0 : 1,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Fight!'),
      ),
      body: Observer(builder: (_) {
        final creature = gameState.ownPlayer.activeCreature;
        print(creature);

        // return Text('creature selected: ${creature.maxHealth}');

        return Container(
            color: ElementColors[creature.element],
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Container(
                    color: Colors.white54,
                    child: Column(children: <Widget>[
                      Text(
                        creature.element.toString(),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        'Stats',
                        textAlign: TextAlign.center,
                      ),
                      Wrap(
                        spacing: 10,
                        runSpacing: 10,
                        runAlignment: WrapAlignment.spaceEvenly,
                        children: <Widget>[
                          Text(
                            'HP: ${creature.health}/${creature.maxHealth}',
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            'Stamina: ${creature.stamina}/${creature.maxStamina}',
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                      Text(
                        'Power',
                        textAlign: TextAlign.center,
                      ),
                      Wrap(
                        spacing: 10,
                        runSpacing: 10,
                        runAlignment: WrapAlignment.spaceEvenly,
                        children: <Widget>[
                          Text(
                            'ATK: ${creature.atk}',
                            textAlign: TextAlign.center,
                          ),
                          Text('DEF: ${creature.def}'),
                        ],
                      )
                    ])),
                creature.dead
                    ? Center(child: Text('DEAD :('))
                    : Wrap(
                        spacing: 10,
                        runSpacing: 10,
                        runAlignment: WrapAlignment.spaceEvenly,
                        children: creature.moves.map<Widget>((move) {
                          return RaisedButton(
                            child: Container(
                                padding: EdgeInsets.all(4.0),
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      move.name,
                                      textAlign: TextAlign.center,
                                    ),
                                    Text(move.description),
                                  ],
                                )),
                            onPressed: () => print('move ${move.name}'),
                            autofocus: false,
                            clipBehavior: Clip.none,
                          );
                        }).toList(),
                      )
              ],
            ));
      }),
    );
  }
}
