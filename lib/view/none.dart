import 'package:flutter/material.dart';

import 'package:bacongamejam20198/stores/all.dart';

class NoneView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Text('Start'),
              autofocus: false,
              clipBehavior: Clip.none,
              onPressed: applicationState.start,
            ),
            RaisedButton(
              child: Text('Join'),
              autofocus: false,
              clipBehavior: Clip.none,
              onPressed: applicationState.join,
            )
          ],
        ),
      ),
    );
  }
}
