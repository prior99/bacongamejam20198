import 'package:bacongamejam20198/network/main.dart';
import 'package:bacongamejam20198/utils/random.dart';
import 'package:mobx/mobx.dart';

// Include generated file
part 'application-state.g.dart';

enum ApplicationState { error, none, joining, joined, ready, fighting, over }

// This is the class used by rest of your codebase
class ApplicationStateStore = _ApplicationStateStore
    with _$ApplicationStateStore;

// The store-class
abstract class _ApplicationStateStore with Store {
  final _network = Network();

  @observable
  ApplicationState current = ApplicationState.none;

  @observable
  bool isHost;

  @observable
  String gameId;

  @observable
  bool hasOpponent = false;

  @observable
  bool isOpponentReady = false;

  @action
  void set(ApplicationState newState) {
    current = newState;
  }

  @action
  Future<void> start() async {
    set(ApplicationState.joining);

    isHost = true;
    gameId = '10'; // anyInt().toString();
    await _network.join(gameId);

    set(ApplicationState.joined);
  }

  @action
  join() async {
    if (gameId != null) {
      await _network.join(gameId);
      set(ApplicationState.joined);
    } else {
      set(ApplicationState.joining);

      isHost = false;
    }
  }

  @action
  updateGameId(String newId) {
    gameId = newId;
  }

  @action
  announceOpponent() {
    hasOpponent = true;
  }

  @action
  setOpponentReady(bool ready) {
    isOpponentReady = ready;

    if (current == ApplicationState.ready && isOpponentReady) {
      set(ApplicationState.fighting);
    }
  }

  @action
  ready() {
    set(ApplicationState.ready);

    if (isOpponentReady) {
      set(ApplicationState.fighting);
    }

    _network.setReady();
  }
}
