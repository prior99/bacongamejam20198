import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/generator/generate-creature.dart';
import 'package:mobx/mobx.dart';

// Include generated file
part 'creatures-state.g.dart';

// This is the class used by rest of your codebase
class CreaturesStore = _CreaturesStore with _$CreaturesStore;

// The store-class
abstract class _CreaturesStore with Store {
  ObservableList<Creature> creatures = ObservableList<Creature>();

  @computed
  bool get complete {
    return creatures.length == 4;
  }

  @action
  void addCreature(Creature creature) {
    this.creatures.add(creature);
  }

  @action
  void addCreatureFromSeed(int seed) {
    this.creatures.add(generateCreature(seed: seed));
  }

  @action
  void removeCreature(Creature creature) {
    this.creatures.remove(creature);
  }
}
