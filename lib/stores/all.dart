import 'application-state.dart';

import 'package:bacongamejam20198/stores/creatures-state.dart';

import 'game-state.dart';

final gameState = GameStateStore();
final creatures = CreaturesStore();
final applicationState = ApplicationStateStore();
