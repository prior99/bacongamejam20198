import 'package:bacongamejam20198/domain/game.dart';
import 'package:bacongamejam20198/domain/player.dart';
import 'package:mobx/mobx.dart';

// Include generated file
part 'game-state.g.dart';

// This is the class used by rest of your codebase
class GameStateStore = _GameStateStore with _$GameStateStore;

// The store-class
abstract class _GameStateStore with Store {
  @observable
  Game game = null;

  @observable
  int ownPlayerIndex = null;

  @action
  void initialize({List<Player> players, int ownPlayerIndex}) {
    this.game = Game(players: players);
    this.ownPlayerIndex = ownPlayerIndex;
  }

  @computed
  bool get started {
    return this.game != null && this.ownPlayerIndex != null;
  }

  @computed
  Player get ownPlayer {
    return this.game.players[this.ownPlayerIndex];
  }

  @computed
  Player get otherPlayer {
    return this.game.players.firstWhere((player) => player != this.ownPlayer);

  }
}
