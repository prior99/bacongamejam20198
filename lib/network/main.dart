import 'package:bacongamejam20198/network/messages.dart';
import 'package:bacongamejam20198/stores/all.dart';
import 'package:bacongamejam20198/stores/application-state.dart';
import 'package:bacongamejam20198/utils/random.dart';
import 'package:mqtt_client/mqtt_client.dart';

const DEBUG = false;

const BROKER = 'test.mosquitto.org';
const BASE_IDENTIFIER = 'bacongamejam20198';
const BASE_TOPIC = '$BASE_IDENTIFIER/game';
const BASE_CLIENT_ID = '$BASE_IDENTIFIER/client';
const QOS = MqttQos.atLeastOnce;

class Network {
  final clientId = '$BASE_CLIENT_ID/${anyInt()}';

  String gameTopic;
  MqttClient _client;

  Network() {
    _client = MqttClient(BROKER, clientId);
  }

  Future<void> join(String id) async {
    gameTopic = '$BASE_TOPIC/$id';

    _client.logging(on: DEBUG);
    try {
      await _client.connect();
    } on Exception catch (e) {
      print('NETWORK::connection exception - $e');
      _client.disconnect();
      return;
    }

    if (_client.connectionStatus.state == MqttConnectionState.connected) {
      print('NETWORK::INFO Mosquitto client connected as "$clientId"');
    } else {
      /// Use status here rather than state if you also want the broker return code.
      print(
          'NETWORK::ERROR Mosquitto client connection failed - disconnecting, status is "${_client.connectionStatus}"');
      _client.disconnect();
      applicationState.set(ApplicationState.error);
      return;
    }

    _client.subscribe(gameTopic, QOS);
    _client.updates.listen(_handleMessage,
        onError: (error) => print('NETWORK::ERROR $error'));

    _client.publishMessage(gameTopic, QOS, Messages.join(clientId));
  }

  setReady() async {
    _sendMessage(Messages.setReady(clientId));
  }

  _sendMessage(dynamic msg) {
    _client.publishMessage(gameTopic, QOS, msg);
  }

  _handleMessage(List<MqttReceivedMessage<MqttMessage>> rawMessages) {
    for (MqttReceivedMessage<MqttMessage> currentRawMessage in rawMessages) {
      final MqttPublishMessage payloadMessage = currentRawMessage.payload;
      final String data = MqttPublishPayload.bytesToStringAsString(
          payloadMessage.payload.message);

      final message = Messages.getMessage(data);
      if (message.clientId == clientId) {
        return;
      }

      print("recieve message $data");

      if (message is JoinMessage) {
        applicationState.announceOpponent();
        _sendMessage(Messages.welcome(clientId,
            ready: applicationState.current.index >=
                ApplicationState.ready.index));
      } else if (message is WelcomeMessage) {
        applicationState.announceOpponent();
        applicationState.setOpponentReady(message.ready);
      } else if (message is SetReadyMessage) {
        applicationState.setOpponentReady(true);
      }
    }
  }
}
