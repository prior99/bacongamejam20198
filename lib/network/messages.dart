import 'dart:convert';

import 'package:mqtt_client/mqtt_client.dart';

class MessageTypes {
  static const join = 'join';
  static const welcome = 'welcome';
  static const setReady = 'ready';
}

class JoinMessage extends Message {
  JoinMessage(String clientId) : super(clientId, MessageTypes.join);
}

class WelcomeMessage extends Message {
  static const READY = 'ready';

  bool ready;

  WelcomeMessage(String clientId, {this.ready}) : super(clientId, MessageTypes.welcome);

  Map<String, dynamic> create() {
    var message = super.create();

    message[WelcomeMessage.READY] = ready;

    return message;
  }
}

class SetReadyMessage extends Message {
  SetReadyMessage(String clientId) : super(clientId, MessageTypes.setReady);
}

class Message {
  static const TYPE = 'type';
  static const CLIENT_ID = 'clientId';

  final String clientId;
  final String type;

  Message(this.clientId, this.type);

  Map<String, dynamic> create() {
    return {
      Message.CLIENT_ID: clientId,
      Message.TYPE: type,
    };
  }

  static Message fromMap(Map data) {
    final clientId = data[Message.CLIENT_ID];

    switch (data[Message.TYPE]) {
      case MessageTypes.join:
        return new JoinMessage(clientId);
      case MessageTypes.welcome:
        return new WelcomeMessage(clientId, ready: data[WelcomeMessage.READY]);
      case MessageTypes.setReady:
        return new SetReadyMessage(clientId);
      default:
        throw new Exception('Unkown message of type "${data[Message.TYPE]}"');
    }
  }
}

class Messages {
  static final _toJSON = JsonEncoder();
  static final _fromJSON = JsonDecoder();

  static dynamic join(String clientId) {
    return _serialize(JoinMessage(clientId));
  }

  static dynamic welcome(String clientId, {bool ready}) {
    return _serialize(WelcomeMessage(clientId, ready: ready));
  }

  static dynamic setReady(String clientId) {
    return _serialize(SetReadyMessage(clientId));
  }

  static Message getMessage(String data) {
    return Message.fromMap(_fromJSON.convert(data) as Map);
  }

  static dynamic _serialize(Message msg) {
    var payload = _toJSON.convert(msg.create());

    final MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();
    builder.addString(payload);
    
    return builder.payload;
  }
}
