import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/effect/effect.dart';
import 'package:bacongamejam20198/domain/event.dart';

class EffectIntervalHeal extends Effect {
  final int duration;
  final int amount;
  final String _name;

  int turns;

  EffectIntervalHeal({
    this.duration,
    Creature target,
    this.amount,
    String name,
  }) : turns = 0, _name = name;

  @override
  onEvent({Event event, Creature target}) {
    if (event.eventType == EventType.TURN_START) {
      this.turns++;
      target.heal(this.amount);
      if (this.turns > this.duration) {
        target.removeEffect(this);
      }
    }
  }

  int get turnsLeft {
    return this.duration - this.turns;
  }

  @override
  String description({Creature target}) {
    return "Heals ${this.amount} each turn for up to ${this.duration} turns. " +
        "(${this.turnsLeft} turns left)";
  }

  @override
  String name({Creature target}) {
    return this._name;
  }
}
