import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/effect/effect.dart';
import 'package:bacongamejam20198/domain/event.dart';

class EffectCharmed extends Effect {
  final int duration;
  final int amount;

  int turns;

  EffectCharmed({this.duration, this.amount}) : turns = 0;

  @override
  onEvent({Event event, Creature target}) {
    if (event.eventType == EventType.TURN_START) {
      this.turns++;
      if (this.turns > this.duration) {
        target.removeEffect(this);
      }
    }
  }

  int get turnsLeft {
    return this.duration - this.turns;
  }

  @override
  int changeAtk({ Creature target }) {
    return this.amount;
  }

  @override
  String description({ Creature target }) {
    return "Lowers ATK by ${this.amount} for up to ${this.duration} turn(s). " +
        "(${this.turnsLeft} turns left)";
  }

  @override
  String name({ Creature target }) {
    return "Charmed";
  }
}
