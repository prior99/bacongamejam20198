import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/effect/effect.dart';
import 'package:bacongamejam20198/domain/event.dart';

class EffectStunned extends Effect {
  final int duration;

  int turns;

  EffectStunned({this.duration, Creature target}) : turns = 0;

  @override
  onEvent({Event event, Creature target}) {
    if (event.eventType == EventType.TURN_START) {
      this.turns++;
      target.stamina = 0;
      if (this.turns > this.duration) {
        target.removeEffect(this);
      }
    }
  }

  int get turnsLeft {
    return this.duration - this.turns;
  }

  @override
  String description({Creature target}) {
    return "Sets stamina to 0 for up to ${this.duration} turn(s). " +
        "(${this.turnsLeft} turns left)";
  }

  @override
  String name({Creature target}) {
    return "Stunned";
  }
}
