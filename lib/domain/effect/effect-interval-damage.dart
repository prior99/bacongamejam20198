import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/damage.dart';
import 'package:bacongamejam20198/domain/effect/effect.dart';
import 'package:bacongamejam20198/domain/event.dart';

class EffectIntervalDamage extends Effect {
  final int duration;
  final Damage damage;
  final String _name;

  int turns;

  EffectIntervalDamage({
    this.duration,
    Creature target,
    this.damage,
    String name,
  })  : turns = 0, _name = name;

  @override
  onEvent({Event event, Creature target}) {
    if (event.eventType == EventType.TURN_START) {
      this.turns++;
      target.inflictDamage(this.damage);
      if (this.turns > this.duration) {
        target.removeEffect(this);
      }
    }
  }

  int get turnsLeft {
    return this.duration - this.turns;
  }

  @override
  String description({Creature target}) {
    return "Deals ${this.damage} each turn for up to ${this.duration} turns. " +
        "(${this.turnsLeft} turns left)";
  }
  @override
  String name({Creature target}) {
    return this._name;
  }
}
