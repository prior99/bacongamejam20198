import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/event.dart';

class Effect {
  onEvent({Event event, Creature target}) {}

  int changeMaxStamina({Creature target}) {
    return 0;
  }

  int changeMaxHealth({Creature target}) {
    return 0;
  }

  int changeAtk({Creature target}) {
    return 0;
  }

  int changeDef({Creature target}) {
    return 0;
  }

  String name({Creature target}) {
    return "Useless effect";
  }

  String description({Creature target}) {
    return "This effect does nothing.";
  }
}
