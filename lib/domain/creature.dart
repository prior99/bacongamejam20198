import 'dart:math';

import 'package:bacongamejam20198/domain/effect/effect.dart';
import 'package:bacongamejam20198/domain/element.dart';
import 'package:bacongamejam20198/domain/event.dart';
import 'package:bacongamejam20198/domain/move/move.dart';
import 'damage.dart';

class Creature {
  final Element element;
  final int baseMaxStamina;
  final int baseMaxHealth;
  final int baseAtk;
  final int baseDef;

  int stamina;
  int health;
  List<Effect> effects = [];
  List<Move> moves;

  Creature({
    this.moves,
    this.element,
    this.baseMaxStamina,
    this.baseMaxHealth,
    this.baseDef,
    this.baseAtk,
  })  : stamina = baseMaxStamina,
        health = baseMaxHealth;

  summon() {
    this._executeEvent(EventSummoned());
  }

  dismiss() {
    this._executeEvent(EventDismiss());
  }

  executeMove(Move move) {
    this._executeEvent(EventExecuteMove(move: move));
  }

  inflictDamage(Damage damage) {
    this._executeEvent(EventDamage(damage: damage));
    this.health -= max(
        0,
        damage.amount *
            damageMultipler(from: damage.element, to: this.element));
    if (this.health <= 0) {
      this._executeEvent(EventPreDeath());
    }
  }

  heal(int amount) {
    this.health = min(this.maxHealth, this.health + amount);
  }

  pass() {
    this._executeEvent(EventPass());
  }

  turnStart() {
    this._executeEvent(EventTurnStart());
  }

  turnEnd() {
    this._executeEvent(EventTurnEnd());
  }

  _executeEvent(Event event) {
    this
        .effects
        .forEach((effect) => effect.onEvent(event: event, target: this));
  }

  int _accumulateModifiers(Iterable<int> modifiers) {
    return modifiers.length == 0
        ? 0
        : modifiers.reduce((result, change) => result + change);
  }

  int get def {
    return this.baseDef +
        _accumulateModifiers(
            this.effects.map((effect) => effect.changeDef(target: this)));
  }

  int get atk {
    return this.baseAtk +
        _accumulateModifiers(
            this.effects.map((effect) => effect.changeAtk(target: this)));
  }

  int get maxStamina {
    return this.baseMaxStamina +
        _accumulateModifiers(this
            .effects
            .map((effect) => effect.changeMaxStamina(target: this)));
  }

  int get maxHealth {
    return this.baseMaxHealth +
        _accumulateModifiers(
            this.effects.map((effect) => effect.changeMaxHealth(target: this)));
  }

  bool get dead {
    return this.health <= 0;
  }

  void addEffect(Effect effect) {
    this.effects.add(effect);
  }

  void removeEffect(Effect effect) {
    this.effects.remove(effect);
  }
}
