import 'package:bacongamejam20198/domain/element.dart';

class Damage {
  Element element;
  int amount;

  Damage({this.element, this.amount});

  @override
  toString() {
    return "${this.amount} ${elementToString(this.element)} damage";
  }
}
