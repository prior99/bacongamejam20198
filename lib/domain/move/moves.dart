import 'package:bacongamejam20198/domain/element.dart';
import 'package:bacongamejam20198/domain/move/move-bandage.dart';
import 'package:bacongamejam20198/domain/move/move-block.dart';
import 'package:bacongamejam20198/domain/move/move-cute-look.dart';
import 'package:bacongamejam20198/domain/move/move-electrical-discharge.dart';
import 'package:bacongamejam20198/domain/move/move-poison.dart';
import 'package:bacongamejam20198/domain/move/move-punch.dart';
import 'package:bacongamejam20198/domain/move/move-tackle.dart';

import 'move.dart';

final vegetableMoves = [
  MovePoison(),
];

final humanMoves = [
  MoveBandage(),
];

final chaosMoves = [
  MoveElectricalDischarge(),
];

final animalMoves = [
  MoveCuteLook(),
];

final physicalMoves = [
  MovePunch(),
  MoveTackle(),
  MoveBlock(),
];

final all = [
  ...vegetableMoves,
  ...humanMoves,
  ...chaosMoves,
  ...animalMoves,
  ...physicalMoves,
];

List<Move> movesForElement(Element element) {
  switch (element) {
    case Element.HUMAN:
      return [...humanMoves, ...physicalMoves];
    case Element.ANIMAL:
      return [...animalMoves, ...physicalMoves];
    case Element.VEGETABLE:
      return [...vegetableMoves, ...physicalMoves];
    case Element.CHAOS:
      return [...chaosMoves, ...physicalMoves];
    default:
      return [...physicalMoves];
  }
}

int moveToInt(Move move) {
  return all.indexOf(move);
}

Move moveFromInt(int index) {
  return all[index];
}