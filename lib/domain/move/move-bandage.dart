import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/damage.dart';
import 'package:bacongamejam20198/domain/effect/effect-interval-heal.dart';
import 'package:bacongamejam20198/domain/element.dart';
import 'move.dart';

class MoveBandage extends Move {
  final String name = "Bandage";
  final String description =
      "Apply an effect that heals 5 HP each turn for 3 turns. Useless as defense.";

  MoveBandage();

  @override
  Damage attack({Creature caster, Creature target}) {
    caster.addEffect(EffectIntervalHeal(
      target: target,
      amount: 5,
      name: "Bandaged",
    ));
    return Damage(amount: 0, element: Element.NONE);
  }
}

