import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/damage.dart';
import 'package:bacongamejam20198/domain/effect/effect-stunned.dart';
import 'package:bacongamejam20198/domain/element.dart';
import 'move.dart';

class MoveElectricalDischarge extends Move {
  final String name = "Electrical Discharge";
  final String description =
      "Apply stunned for one turn (Sets stamina to 0). Useless as defense.";

  MoveElectricalDischarge();

  @override
  Damage attack({Creature caster, Creature target}) {
    target.addEffect(EffectStunned(
      target: target,
      duration: 1,
    ));
    return Damage(amount: 0, element: Element.NONE);
  }
}
