import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/damage.dart';
import 'package:bacongamejam20198/domain/element.dart';
import 'move.dart';

class MovePunch extends Move {
  final String name = "Punch";
  final String description =
      "Defends for 20% of DEF. Deals full ATK as non-elemental damage.";

  MovePunch();

  @override
  defend({Damage damage, Creature caster, Creature target}) {
    damage.amount -= (caster.def.toDouble() * 0.2).toInt();
  }

  @override
  attack({Creature caster, Creature target}) {
    return Damage(amount: caster.atk, element: Element.NONE);
  }
}
