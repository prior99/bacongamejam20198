import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/damage.dart';
import 'package:bacongamejam20198/domain/element.dart';


class Move {
  Move();

  Element get element {
    return Element.NONE;
  }

  bool condition({Creature caster, Creature target}) {
    return true;
  }

  Damage attack({Creature caster, Creature target}) {
    return Damage(amount: 0, element: Element.NONE);
  }

  defend({Damage damage, Creature caster, Creature target}) {
    return;
  }

  String get name {
    return "Useless action";
  }

  String get description {
    return "This action will do nothing.";
  }
}
