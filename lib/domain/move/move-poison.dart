import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/damage.dart';
import 'package:bacongamejam20198/domain/effect/effect-interval-damage.dart';
import 'package:bacongamejam20198/domain/element.dart';
import 'move.dart';

class MovePoison extends Move {
  final String name = "Posion";
  final String description =
      "Apply an effect that deals 20% of ATK each turn for 3 turns. Useless as defense.";

  MovePoison();

  @override
  Damage attack({Creature caster, Creature target}) {
    target.addEffect(EffectIntervalDamage(
      target: target,
      damage:
          Damage(amount: (caster.atk * 0.2) as int, element: Element.VEGETABLE),
      name: "Poisioned",
    ));
    return Damage(amount: 0, element: Element.NONE);
  }
}
