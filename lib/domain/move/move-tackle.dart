import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/damage.dart';
import 'package:bacongamejam20198/domain/element.dart';
import 'move.dart';

class MoveTackle extends Move {
  final String name = "Tackle";
  final String description =
      "Deals 70% of ATK as physical damage and defends for 50% of DEF.";

  MoveTackle();

  @override
  defend({Damage damage, Creature caster, Creature target}) {
    damage.amount -= (caster.def.toDouble() * 0.5).toInt();
  }

  @override
  attack({Creature caster, Creature target}) {
    return Damage(
        amount: (caster.atk.toDouble() * 0.7).toInt(), element: Element.NONE);
  }
}
