import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/damage.dart';
import 'package:bacongamejam20198/domain/element.dart';
import 'move.dart';

class MoveBlock extends Move {
  final String name = "Block";
  final String description =
      "Blocks the incoming attack with 100% of DEF and attacks with 20% of ATK.";

  MoveBlock();

  @override
  defend({Damage damage, Creature caster, Creature target}) {
    damage.amount -= caster.def;
  }

  @override
  attack({Creature caster, Creature target}) {
    return Damage(
        amount: (caster.atk.toDouble() * 0.2).toInt(), element: Element.NONE);
  }
}
