import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/damage.dart';
import 'package:bacongamejam20198/domain/effect/effect-charmed.dart';
import 'package:bacongamejam20198/domain/element.dart';
import 'move.dart';

class MoveCuteLook extends Move {
  final String name = "Cute Look";
  final String description =
      "Lowers opponents ATK by 20% of own ATK for 5 turns. Defends for 20% of DEF.";

  MoveCuteLook();

  @override
  defend({Damage damage, Creature caster, Creature target}) {
    damage.amount -= (caster.def.toDouble() * 0.2).toInt();
  }

  @override
  Damage attack({Creature caster, Creature target}) {
    target.addEffect(EffectCharmed(
      amount: caster.atk,
      duration: 5,
    ));
    return Damage(amount: 0, element: Element.NONE);
  }
}
