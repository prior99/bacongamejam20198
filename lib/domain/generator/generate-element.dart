import 'dart:math';

import 'package:bacongamejam20198/domain/element.dart';

const List<Element> elements = [Element.ANIMAL, Element.CHAOS, Element.HUMAN, Element.VEGETABLE];

Element generateElement({ int seed}) {
  return elements[Random(seed).nextInt(elements.length)];
}