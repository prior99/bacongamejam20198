import 'dart:math';

import 'package:bacongamejam20198/domain/creature.dart';
import 'package:bacongamejam20198/domain/generator/generate-element.dart';
import 'package:bacongamejam20198/domain/generator/generate-move-set.dart';

const totalAtkDef = 20;
const minAtkDef = 2;
const totalHealthStamina = 150;
const minMaxStamina = 4;
const maxMaxStamina = 8;
const staminaToHealthRatio = 10;

Creature generateCreature({int seed}) {
  final element = generateElement(seed: seed);
  final moves = generateMoveSet(element: element, seed: seed);
  final rand = Random(seed);
  final baseAtk = rand.nextInt(totalAtkDef - 2 * minAtkDef) + minAtkDef;
  final baseDef = totalAtkDef - baseAtk;
  final baseMaxStamina = rand.nextInt(maxMaxStamina - minMaxStamina) + minMaxStamina;
  final baseMaxHealth = totalHealthStamina - baseMaxStamina * staminaToHealthRatio;
  return Creature(
    moves: moves,
    element: element,
    baseMaxHealth: baseMaxHealth,
    baseMaxStamina: baseMaxStamina,
    baseAtk: baseAtk,
    baseDef: baseDef,
  );
}
