import 'dart:math';

import 'package:bacongamejam20198/domain/element.dart';
import 'package:bacongamejam20198/domain/move/move.dart';
import 'package:bacongamejam20198/domain/move/moves.dart';

List<Move> generateMoveSet({Element element, int seed}) {
  final rand = Random(seed);
  final options = movesForElement(element);
  final List<Move> result = [];
  while (result.length < 4 && options.length > 0) {
    final index = rand.nextInt(options.length);
    final move = options.removeAt(index);
    result.add(move);
  }
  return result;
}