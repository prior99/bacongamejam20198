import 'package:bacongamejam20198/domain/action.dart';
import 'package:bacongamejam20198/domain/player.dart';

class Game {
  final List<Player> players;

  int turnCount = 0;

  Game({this.players});

  performTurn(List<Action> actions) {
    if (actions.length != this.players.length) {
      throw Exception("Invalid action count.");
    }
    final playerWithoutAction = players.firstWhere(
      (player) =>
          actions.firstWhere(
            (action) => action.casterIndex == this.players.indexOf(player),
            orElse: () => null,
          ) ==
          null,
      orElse: () => null,
    );
    if (playerWithoutAction != null) {
      throw Exception("Missing action for at least one player.");
    }
    // Emit all events for turn start on the active creatures.
    this
        .players
        .map((player) => player.activeCreature)
        .forEach((creature) => creature.turnStart());
    actions.sort((a, b) => a.compareTo(b));
    actions.forEach((action) {
      final caster = this.players[action.casterIndex];
      switch (action.actionType) {
        case ActionType.SURRENDER:
          caster.surrendered = true;
          return;
        case ActionType.PASS:
          // Emit event for passing on active creature.
          caster.activeCreature.pass();
          break;
        case ActionType.CHANGE_CREATURE:
          // Emit event for dismissing on active creature.
          caster.activeCreature.dismiss();
          caster.activeCreatureIndex = (action as ActionChangeCreature).creatureIndex;
          // Emit event for summoning on active creature.
          caster.activeCreature.summon();
          break;
        case ActionType.MOVE:
          final actionMove = action as ActionMove;
          final move = caster.activeCreature.moves[actionMove.moveIndex];
          // Emit event for executing the move on active creature.
          caster.activeCreature.executeMove(move);
          break;
      }
    });
    // Execute the real action logic in a second pass.
    actions
        .where((action) => action.actionType == ActionType.MOVE)
        .forEach((action) {
      final caster = this.players[action.casterIndex];
      final actionMove = action as ActionMove;
      final move = caster.activeCreature.moves[actionMove.moveIndex];
      final opponent = this.players.firstWhere((player) => player != caster);
      final damage = move.attack(
          caster: caster.activeCreature, target: opponent.activeCreature);
      actions
          .where((action) => action.actionType == ActionType.MOVE)
          .where((action) => action != actionMove)
          .forEach((action) => this
              .players[action.casterIndex]
              .activeCreature
              .moves[(action as ActionMove).moveIndex]
              .defend(
                damage: damage,
                caster: caster.activeCreature,
                target: opponent.activeCreature,
              ));
      opponent.activeCreature.inflictDamage(damage);
    });
    // Emit all events for turn end on the active creatures.
    this
        .players
        .map((player) => player.activeCreature)
        .forEach((creature) => creature.turnEnd());
  }

  bool get gameOver {
    return this
            .players
            .firstWhere((player) => player.defeated, orElse: () => null) !=
        null;
  }

  bool get tie {
    if (!this.gameOver) {
      return false;
    }
    return this
            .players
            .firstWhere((player) => !player.defeated, orElse: () => null) ==
        null;
  }

  Player get winner {
    if (!this.gameOver || this.tie) {
      return null;
    }
    return this
        .players
        .firstWhere((player) => !player.defeated, orElse: () => null);
  }

  Player get loser {
    if (!this.gameOver || this.tie) {
      return null;
    }
    return this
        .players
        .firstWhere((player) => player.defeated, orElse: () => null);
  }
}
