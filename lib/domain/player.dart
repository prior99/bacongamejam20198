import 'package:bacongamejam20198/domain/creature.dart';

class Player {
  final List<Creature> creatures;

  int activeCreatureIndex = 0;

  bool surrendered = false;

  Player({this.creatures});

  Creature get activeCreature {
    return this.creatures[this.activeCreatureIndex];
  }

  bool get defeated {
    if (this.surrendered) {
      return true;
    }
    return this
            .creatures
            .firstWhere((creature) => !creature.dead, orElse: () => null) ==
        null;
  }
}
