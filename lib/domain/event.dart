import 'package:bacongamejam20198/domain/damage.dart';
import 'package:bacongamejam20198/domain/move/move.dart';

enum EventType {
  SUMMONED,
  PRE_DEATH,
  DISMISS,
  EXECUTE_MOVE,
  PASS,
  TURN_START,
  TURN_END,
  DAMAGE,
}



class Event {
  final EventType eventType;

  Event({this.eventType});
}

class EventSummoned extends Event {
  EventSummoned() : super(eventType: EventType.SUMMONED);
}

class EventPreDeath extends Event {
  EventPreDeath() : super(eventType: EventType.PRE_DEATH);
}

class EventDismiss extends Event {
  EventDismiss() : super(eventType: EventType.DISMISS);
}

class EventExecuteMove extends Event {
  final Move move;
  EventExecuteMove({this.move}) : super(eventType: EventType.EXECUTE_MOVE);
}

class EventPass extends Event {
  EventPass() : super(eventType: EventType.PASS);
}

class EventTurnStart extends Event {
  EventTurnStart() : super(eventType: EventType.TURN_START);
}

class EventTurnEnd extends Event {
  EventTurnEnd() : super(eventType: EventType.TURN_END);
}

class EventDamage extends Event {
  final Damage damage;
  EventDamage({this.damage}) : super(eventType: EventType.DAMAGE);
}
