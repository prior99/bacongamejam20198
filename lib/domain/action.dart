import 'package:json_annotation/json_annotation.dart';

part 'action.g.dart';

enum ActionType {
  MOVE,
  PASS,
  CHANGE_CREATURE,
  SURRENDER,
}

const actionPriorities = [
  ActionType.SURRENDER,
  ActionType.PASS,
  ActionType.MOVE,
  ActionType.CHANGE_CREATURE
];


@JsonSerializable(explicitToJson: true)
class Action implements Comparable {

  final ActionType actionType;
  final int casterIndex;

  Action({this.actionType, this.casterIndex});

  static fromJson(Map<String, dynamic> json) {
    final genericAction = _$ActionFromJson(json);
    switch (genericAction.actionType) {
      case ActionType.MOVE: return ActionMove.fromJson(json);
      case ActionType.PASS: return ActionPass.fromJson(json);
      case ActionType.CHANGE_CREATURE: return ActionChangeCreature.fromJson(json);
      case ActionType.SURRENDER: return ActionSurrender.fromJson(json);
    }
  }

  Map<String, dynamic> toJson() => _$ActionToJson(this);

  @override
  int compareTo(other) {
    return actionPriorities.indexOf(this.actionType) -
        actionPriorities.indexOf(other.actionType);
  }
}

@JsonSerializable(explicitToJson: true)
class ActionMove extends Action {
  final int moveIndex;

  factory ActionMove.fromJson(Map<String, dynamic> json) => _$ActionMoveFromJson(json);

  @override
  Map<String, dynamic> toJson() => { ...super.toJson(), ..._$ActionMoveToJson(this) };

  ActionMove({int casterIndex, this.moveIndex})
      : super(actionType: ActionType.MOVE, casterIndex: casterIndex);
}

@JsonSerializable(explicitToJson: true)
class ActionPass extends Action {
  ActionPass({int casterIndex})
      : super(actionType: ActionType.PASS, casterIndex: casterIndex);

  factory ActionPass.fromJson(Map<String, dynamic> json) => _$ActionPassFromJson(json);

  @override
  Map<String, dynamic> toJson() => { ...super.toJson(), ..._$ActionPassToJson(this) };
}

@JsonSerializable(explicitToJson: true)
class ActionChangeCreature extends Action {
  final int creatureIndex;
  ActionChangeCreature({int casterIndex, this.creatureIndex})
      : super(actionType: ActionType.CHANGE_CREATURE, casterIndex: casterIndex);

  factory ActionChangeCreature.fromJson(Map<String, dynamic> json) => _$ActionChangeCreatureFromJson(json);

  @override
  Map<String, dynamic> toJson() => { ...super.toJson(), ..._$ActionChangeCreatureToJson(this) };
}

@JsonSerializable(explicitToJson: true)
class ActionSurrender extends Action {
  ActionSurrender({int casterIndex})
      : super(actionType: ActionType.SURRENDER, casterIndex: casterIndex);

  factory ActionSurrender.fromJson(Map<String, dynamic> json) => _$ActionSurrenderFromJson(json);

  @override
  Map<String, dynamic> toJson() => { ...super.toJson(), ..._$ActionSurrenderToJson(this) };
}
