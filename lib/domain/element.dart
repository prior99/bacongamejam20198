enum Element {
  HUMAN,
  ANIMAL,
  VEGETABLE,
  CHAOS,
  NONE,
}

int damageMultipler({Element from, Element to}) {
  switch (from) {
    case Element.HUMAN:
      return to == Element.ANIMAL ? 2 : 1;
    case Element.ANIMAL:
      return to == Element.VEGETABLE ? 2 : 1;
    case Element.VEGETABLE:
      return to == Element.CHAOS ? 2 : 1;
    case Element.CHAOS:
      return to == Element.HUMAN ? 2 : 1;
    default:
      return 1;
  }
}

String elementToString(Element element) {
  switch (element) {
    case Element.HUMAN:
      return "human";
    case Element.ANIMAL:
      return "animal";
    case Element.VEGETABLE:
      return "vegetable";
    case Element.CHAOS:
      return "chaos";
    default:
      return "physical";
  }
}
