import 'package:bacongamejam20198/stores/application-state.dart';
import 'package:bacongamejam20198/view/fight.dart';
import 'package:bacongamejam20198/view/pre-game.dart';
import 'package:bacongamejam20198/view/ready.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'stores/all.dart';
import 'view/none.dart';
import 'view/join.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      switch (applicationState.current) {
        case ApplicationState.none:
          return NoneView();
        case ApplicationState.joining:
          return applicationState.isHost
              ? Text('Creating game...')
              : JoinView();
        case ApplicationState.joined:
          return PreGame();
        case ApplicationState.ready:
          return ReadyView();
        case ApplicationState.fighting:
          return FightView();
        case ApplicationState.over:
          // TODO: Handle this case.
          break;
        case ApplicationState.error:
          // TODO: Handle this case.
          break;
      }
      return Text('Not yet implemented: ${applicationState.current}');
    });
  }
}
